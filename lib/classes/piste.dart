import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;

class Piste {
  var piste = new List<CircleMarker>();

  Piste({double lat, double lon}) {
    piste.add(CircleMarker(
      //radius marker
      point: LatLng(lat, lon),
      color: Colors.yellow.withOpacity(1.0),
      borderStrokeWidth: 3.0,
      //useRadiusInMeter: true,
      //radius: 50, //radius
      //useRadiusInMeter: true,
      radius: 5, //radius
      borderColor: Colors.red,
    ));
    print(piste);
  }
}
